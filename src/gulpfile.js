// Diretorio da base
var path = { 
    dest  : '../public/',
    local : 'localhost/viva-la-ideia/_html/'
}

// Carregando modulos
var gulp = require('gulp'),
    stylus = require('gulp-stylus'),
    minifyCss = require('gulp-minify-css'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    imagemin = require('gulp-imagemin'),
    changed  = require('gulp-changed'),
    browserSync = require('browser-sync'),
    notify = require("gulp-notify");

// Compila Stylus para CSS
gulp.task('stylus', function(){
    gulp.src('../assets/stylus/**/*.styl')
        .pipe( stylus() )
        .pipe( notify('CSS compilado!') )
        .pipe( gulp.dest( '../assets/css') )
});

// Otimiza o CSS
gulp.task('cssMin', function(){
    gulp.src('../assets/.tmp/*.css')
        .pipe( minifyCss() )
        .pipe( gulp.dest( path.dest + 'assets/css/') )
});

// Concatena o CSS
gulp.task('css', ['stylus', 'cssMin'], function() {
  return gulp.src('../assets/css/*.css')
    .pipe( concat('style.min.css') )
    .pipe( gulp.dest( '../assets/.tmp/') );
});

// Imagens Otimizadas
gulp.task('img', function() {
    gulp.src('../assets/img/**/*')
        .pipe(changed( path.dest + 'assets/img'))
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}]
        }))
        .pipe(gulp.dest( path.dest + 'assets/img'));
});

// Copia os vendor
gulp.task('jsVendor', function(){
    gulp.src('../assets/js/vendor/*.js')
        .pipe( changed( path.dest + 'assets/js') )
        .pipe( gulp.dest( path.dest + 'assets/js') );
});

// Concatenando os arquivos
gulp.task('scripts', function() {
  return gulp.src(['../assets/js/bootstrap.min.js', '../assets/js/main.js' ])
    .pipe( concat('script.min.js') )
    .pipe( gulp.dest( '../assets/.tmp/') );
});

// Mimifica o javascript
gulp.task('js', ['scripts'], function() {
  gulp.src('../assets/.tmp/*.js')
    .pipe( uglify() )
    .pipe( notify('JS comprimido e concatenado!') )
    .pipe( gulp.dest( path.dest + 'assets/js') )
});


// Copia as fontes
gulp.task('fonts', function(){
    gulp.src('../assets/fonts/*')
        .pipe( changed( path.dest + 'assets/fonts') )
        .pipe( gulp.dest( path.dest + 'assets/fonts') );
});

// Browser-sync
gulp.task('browser-sync', function() {
    browserSync.init( [ path.dest + 'assets/css/*.css', path.dest + '/img/**', '../assets/stylus/**/*.styl', '../*.php' ], {
        proxy: path.local
    });
});

// Observa os arquivos
gulp.task('watch', ['stylus', 'js', 'browser-sync'], function(){
    gulp.watch( ['../assets/stylus/**/*.styl', '../assets/js/*.js'], ['stylus', 'js']);
});

// Default
gulp.task( 'default', ['stylus', 'js', 'img', 'fonts']);